FROM node:16-alpine

# Create user and set it as non-root
RUN mkdir -p /usr/src/app && chown node:node /usr/src/app
USER node

# Copy or mount node app here
WORKDIR /usr/src/app

# Copy the libraries
COPY package*.json /usr/src/app/

# Install the package
RUN npm install

COPY --chown=node:node . /usr/src/app/

EXPOSE 8080
