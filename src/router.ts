import { createRouter, createWebHistory } from 'vue-router';

import HomePage from './pages/home-page.vue';
import AboutMe from './pages/about-me.vue';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', component: HomePage },
    { path: '/about', component: AboutMe },
  ],
});

export default router;