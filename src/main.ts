import { createApp } from 'vue';
import router from './router';
import App from './App.vue';

// Styling
import './style.scss';

// Add some font icons
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faGithub, faLinkedin } from '@fortawesome/free-brands-svg-icons';
import { faFileLines } from '@fortawesome/free-solid-svg-icons';

library.add(
  faGithub,
  faLinkedin,
  faFileLines,
);

createApp(App)
  .use(router)
  .component('font-awesome-icon', FontAwesomeIcon)
  .mount('#app');
