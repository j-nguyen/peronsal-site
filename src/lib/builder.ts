import type { Project } from './project';

export const personalProjects: Project[] = [
  {
    link: 'https://clipable.jnguyen.dev',
    title: 'Clipable (Streamable-like)',
    description: `
      Streamable-like where you can record your own video clips and share it amongst your friends. It uses Ruby on Rails 
      and GoodJob -- a postgres backed job.
    `,
    skillSets: [
      'Ruby on Rails',
      'VueJS',
      'PostgreSQL',
      'GoodJob',
    ],
  },
  {
    link: null,
    title: 'Plex Home Server',
    description: `
      I run my own Plex Home Server at home with configured docker containers so that they're up to date. It uses watchtower as a
      basis.
    `,
    skillSets: [
      'Docker',
    ],
  },
  {
    link: 'https://github.com/j-nguyen/FreshPlanAPI',
    title: 'FreshPlan - Meetup Service',
    description: `
      A service that lets you meet up with your friends. Fun little project I wanted to do that was developed in Swift. There is also an iOS 
      application with it as well. Uses PostgreSQL as its DB service and OneSignal for push notifications.
    `,
    skillSets: [
      'PostgreSQL',
      'Swift',
      'iOS',
    ],
  },
];

export const workProjects: Project[] = [
  {
    link: 'https://betterappbuilder.com',
    title: 'BetterAppBuilder',
    description: `
      At Alteris Group, I developed a SaaS product that curates an informational app in iOS and Android by using the CMS built by my team.
      It currently uses Ruby on Rails, PostgreSQL and Docker as its web-tech stack. Mobile apps are created natively, through iOS Swift and
      Android Kotlin/Java.
    `,
    skillSets: [
      'Ruby on Rails',
      'VueJS',
      'PostgreSQL',
      'Android',
      'iOS',
      'Swift',
      'Kotlin',
    ],
  },
  {
    link: 'https://apps.apple.com/us/app/super-why-phonics-fair/id735434007',
    title: 'Super Why Phonics Fair',
    description: `
      At Red Piston, I maintained and updated the Android and iOS app for Super Why Phonics Fair for ABC Kids. Features fun mini-games
      such as whack-a-mole, spelling games, and much more.
    `,
    skillSets: [
      'Corona',
      'Android',
      'Lua',
    ],
  },
  {
    link: 'https://covidcomply.com',
    title: 'Covid Compliance',
    description: `
      At Alteris Group, we created a SaaS product to help monitor and manage your day-to-day tasks at work. This product was designed to help
      keep your workplace safe, by tracking your daily health status through questionnaires, having your own custom response plan for
      COVID safety protocols.
    `,
    skillSets: [
      'Ruby on Rails',
      'VueJS',
      'PostgreSQL',
      'iOS',
    ],
  },
];