export interface Project {
  link?: string | null;
  title: string;
  description: string;
  skillSets: string[];
}